const express = require('express')
const { MongoClient } = require('mongodb')
const assert = require('assert')
const app = express()
const port = 3000
const path = require('path');
const res = require('express/lib/response')
const { read } = require('fs')
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

var htmlContent = ""

app.listen(port, () => console.log('Example app listening on port ' + port))

app.get('/', (req, res) => res.send('Hello World!'))

const urlMongoDB = "mongodb://localhost:27017"
const mongoClient = new MongoClient(urlMongoDB)

var db = null;
var todos = null;
var todolist = null;

app.get('/todos', async (req, res) => {
    res.sendFile(path.join(__dirname + '/app.html'))
    try {
        await mongoClient.connect()
        db = mongoClient.db('todolist')
        todos = db.collection("todos")
        createToDo("test")
        listToDo()

        // todolist = await todos.find().toArray()

        // deleteToDo("test", "ceci est un test", false)

        // todolist = await todos.find().toArray()

        //updateToDo("ceci est un test", false, "ceci test update", true)

        // res.sendFile('app.html');
        // res.send(todolist)
    }
    catch (e) {
        console.error(e)
    }
    finally {
        //mongoClient.close()
    }
})

function readHTMLFile() {
    const fs = require("fs");

    const buffer = fs.readFileSync("app.html");
    
    htmlContent = buffer.toString();
    
    global.document = new JSDOM(htmlContent).window.document;
    
    // console.log(htmlContent);
}

async function listToDo() {

    //todolist = await todos.find().toArray()
    todolist = await todos.find({}, { projection: { _id: 0, text: 1, done: 1 } }).toArray()

    readHTMLFile()
    
    for (let i = 0; i < todolist.length; i++) {

        const obj = JSON.parse(JSON.stringify(todolist[i]));

        var myDiv = document.createElement("div");
        myDiv.setAttribute("class", "form-check");

        var myInput = document.createElement("input");
        myInput.setAttribute("class", "form-check-input");
        myInput.setAttribute("type", "checkbox");

        if (obj.done == false)
            myInput.setAttribute("checked", "");
        else
            myInput.setAttribute("checked", "checked");


        var myLabel = document.createElement("label");
        myLabel.setAttribute("class", "form-check-label");
        myLabel.innerHTML = obj.text;

        myDiv.appendChild(myInput);
        myDiv.appendChild(myLabel);

        document.getElementById("tasks").appendChild(myDiv);
    }
}

function addToDo() {

    var myDiv = document.createElement("div");
    myDiv.setAttribute("class", "form-check");

    var myInput = document.createElement("input");
    myInput.setAttribute("class", "form-check-input");
    myInput.setAttribute("id", "task-input");
    myInput.setAttribute("type", "checkbox");
    myInput.setAttribute("checked", "checked");

    var myLabel = document.createElement("label");
    myLabel.setAttribute("class", "form-check-label");
    myLabel.setAttribute("id", "task-label");

    myLabel.innerHTML = "It's a test";

    myDiv.appendChild(myInput);
    myDiv.appendChild(myLabel);

    document.getElementById("tasks").appendChild(myDiv);
}

function createToDo(textVal) {
    var myobj = { text: textVal, done: false };

    todos.insertOne(myobj, function (err, res) {
        if (err) throw err;
        console.log("1 document inserted");
    });
}

function updateToDo(textVal, doneVal, newTextVal, newDoneVal) {
    var myquery = { text: textVal, done: doneVal };
    var newvalues = { $set: { text: newTextVal, done: newDoneVal } };
    todos.updateOne(myquery, newvalues, function (err, res) {
        if (err) throw err;
        console.log("1 document updated");
    });
}

function deleteToDo(textVal, doneVal) {
    var myquery = { text: textVal, done: doneVal };

    todos.deleteMany(myquery, function (err, obj) {
        if (err) throw err;
        // console.log(obj.result.n + " window.document(s) deleted");
    });
}
